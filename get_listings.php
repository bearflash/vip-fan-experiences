<?php

	// Keywords from Query String

	$q = $_POST['q'];

	$searched_lower = strtolower($q);

	$searched = strtoupper($searched_lower);

	$searchedURL = urlencode($searched); 



	$endpoint_stubhub = "http://publicfeed.stubhub.com/listingCatalog/select/";

	

	if(!empty($_POST['ancestorDescriptions'])){

		$ancestorDescriptions = $_POST['ancestorDescriptions'];

	}elseif(empty($_POST['ancestorDescriptions'])){

		$ancestorDescriptions = '';

	}

	

	if(!empty($_POST['sort_what'])){

		$sort_what = $_POST['sort_what'];

	}elseif(empty($_POST['sort_what'])){

		$sort_what = 'event_date_time_local';

	}

	

	if(!empty($_POST['sort_how'])){

		$sort_how = $_POST['sort_how'];

	}elseif(empty($_POST['sort_how'])){

		$sort_how = 'asc';

	}

	

	// StubHub API Query - JSON Response

	$url = "$endpoint_stubhub?q=%252BstubhubDocumentType%253Aevent%250D%250A%252B"

			. "%2Bleaf%253A%2Btrue%250D%250A%252B"

			. "%2Bdescription%253A%2B%22$searchedURL%22%250D%250A%252B"

			. "%3B$sort_what%20$sort_how"

			. "&version=2.2"

			. "&start=0"

			. "&indent=on"

			. "&wt=json"

			. "&fl=description+event_date+event_date_local+event_time_local+geography_parent+venue_name+city+state+genreUrlPath+urlpath+leaf+channel";

	

	

	// Send Request

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_REFERER, "http://www.yourwebsite.com/");

	$body = curl_exec($ch);

	

	curl_close($ch);

	

	// Process JSON string - Convert JSON to PHP Array

	$json = json_decode($body);

		

	// Number of Returned Results

	$num = $json->response->numFound;



	function contains($str, $content, $ignorecase=true){

		if ($ignorecase){

			$str = strtolower($str);

			$content = strtolower($content);

		}  

		return strpos($content,$str) ? true : false;

	}

							

		if ($num > 0){

			// Today's timestamp date

			$today = date(c);

			// Results Loop 

			$i = 0;

			while ($i<$num) {

				// Filter out results with event title "mirror" - StubHub API anomaly

				if(strstr($json->response->docs[$i]->description,"mirror") == false){

				// Filter out results with event title "coming soon" - StubHub API anomaly

				if(strstr($json->response->docs[$i]->description,"coming soon") == false){

				// Filter out results with event title "posted here" - StubHub API anomaly

				if(strstr($json->response->docs[$i]->description,"posted here") == false){

				// Filter out results with event date earlier than today

				if ($json->response->docs[$i]->event_date >= $today)

				{

				// Result format with JSON variables

				$results_events .= "

					<tr>\r\n

						<td valign=\"top\">".$json->response->docs[$i]->description."</td>\r\n

						<td valign=\"top\">".date("F j, Y", strtotime($json->response->docs[$i]->event_date_local))."</td>\r\n

						<td valign=\"top\">".$json->response->docs[$i]->venue_name."</td>

						<td valign=\"top\">".$json->response->docs[$i]->city.", ".$json->response->docs[$i]->state."</td>\r\n

					</tr>\r\n";

				}

				}

				}

				}

			// Loop continuance - finite

			$i++;

			}

			}elseif ($num == 0){

				$results_events .= "

					<tr>\r\n

						<td>There are currently no events listed for your query.</td>\r\n

					</tr>\r\n";

			}

?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>StubHub API - PHP Event Query Sample</title>



<style>

	h1 {

		margin: 10px 0 15px 0;

		font: bold 16px Arial, Helvetica, sans-serif;

		color: #000033;

	}

	form {

		margin: 0 0 15px 0;

	}

	th {

		padding: 5px;

	}

	td {

		padding: 5px;

		font: 11px Arial, Helvetica, sans-serif;

		color: #000000;

	}

</style>



</head>



<body>



<h1>StubHub API - PHP Event Query Sample</h1>



<form action="get_listings.php" method="post">

	<label>Search for Tickets:</label>

    <input type="text" name="q"/>

    <input type="submit" value="Find Tickets"/>

</form>



<table width="750" cellspacing="0" cellpadding="0">

	<thead height="30px" style="background-color:#0099FF; font:bold 14px Arial, Helvetica, sans-serif; color:#FFFFFF;">

    	<th width="300" align="left">EVENT DESCRIPTION</th>

        <th width="100" align="left">EVENT DATE</th>

        <th width="150" align="left">VENUE NAME</th>

        <th width="150" align="left">CITY/STATE</th>

    </thead>

	<?=$results_events?>

</table>



</body>

</html>