<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Lato:700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Raleway:700' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<div id="wrapper" class="hfeed">
		<header id="header">
<!--			<div class="wrapper header">
 				<section id="branding">
					<div id="site-title"><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri()."/images/logo.png" ?>" /></a></div>
				</section> -->
				<nav id="menu" class="paradeiser">
					<a href="<?php echo site_url(); ?>">
						<img class="paradeiser_logo" alt="VIP Fan Experiences, LLC" src="<?php echo get_template_directory_uri()."/images/logo.png" ?>" />
					</a>
					<a href="/nfl-football-travel-packages">
				        <div class="paradeiser_icon_canvas">
				            <img src="<?php echo get_template_directory_uri(); ?>/images/nfl_icon.svg" alt="NFL">
				        </div>
				        <span>NFL</span>
				    </a>
				    <a href="/mlb-travel-packages">
				        <div class="paradeiser_icon_canvas">
				            <img src="<?php echo get_template_directory_uri(); ?>/images/mlb_icon.svg" alt="MLB">
				        </div>
				        <span>MLB</span>
				    </a>
				    <a href="/hockey">
				        <div class="paradeiser_icon_canvas">
				            <img src="<?php echo get_template_directory_uri(); ?>/images/nhl_icon.svg" alt="NHL">
				        </div>
				        <span>NHL</span>
				    </a>
				    <a href="/basketball">
				        <div class="paradeiser_icon_canvas">
				            <img src="<?php echo get_template_directory_uri(); ?>/images/nba_icon.svg" alt="NBA">
				        </div>
				        <span>NBA</span>
				    </a>
				    <a href="/request-a-quote">
				        <div class="paradeiser_icon_canvas">
				            <img src="<?php echo get_template_directory_uri(); ?>/images/request-a-quote-icon.svg" alt="Request a Quote">
				        </div>
				        <span>Get A Quote</span>
				    </a>
				    <a class="paradeiser-hidden-tablet paradeiser-hidden-phone" href="/news">
				        <div class="paradeiser_icon_canvas">
				            <img src="<?php echo get_template_directory_uri(); ?>/images/news_icon.svg" alt="News">
				        </div>
				        <span>News</span>
				    </a>
				    <a class="paradeiser-hidden-tablet paradeiser-hidden-phone" href="/testimonials">
				        <div class="paradeiser_icon_canvas">
				            <img src="<?php echo get_template_directory_uri(); ?>/images/testimonial_icon.svg" alt="Testimonials">
				        </div>
				        <span>Testimonials</span>
				    </a>
					<div class="paradeiser_dropdown">
			            <a class="click-more" href="#">
			                <div class="paradeiser_icon_canvas">
			                    <img src="<?php echo get_template_directory_uri(); ?>/images/more_icon.svg" alt="">
			                </div>
			                <span>More</span>
			            </a>
			            <ul class="paradeiser_children paradeiser_hidden" id="paradeiser-more">
			            	<li class="paradeiser-hidden-desktop paradeiser-hidden-large-desktop"><a href="/news"><img class="svg" src="<?php echo get_template_directory_uri(); ?>/images/news_icon.svg" alt=""><div class="hidden_text">News</div></a></li>
			                <li class="paradeiser-hidden-desktop paradeiser-hidden-large-desktop"><a href="/testimonials"><img class="svg" src="<?php echo get_template_directory_uri(); ?>/images/testimonial_icon.svg" alt=""><div class="hidden_text">Testimonials</div></a></li>
			                <li><a href="tel:1-800-281-3276"><img class="svg" src="<?php echo get_template_directory_uri(); ?>/images/phone_icon.svg" alt=""><div class="hidden_text">Call us!</div></a></li>
			                <li><a href="https://www.facebook.com/vipfanexperiences/" target="_blank"><img class="svg" src="<?php echo get_template_directory_uri(); ?>/images/facebook_icon.svg" alt=""><div class="hidden_text">Facebook</div></a></li>
			                <li><a href="https://twitter.com/vip_fe" target="_blank"><img class="svg" src="<?php echo get_template_directory_uri(); ?>/images/twitter_icon.svg" alt=""><div class="hidden_text">Twitter</div></a></li>
			                <li><a href="https://plus.google.com/+Vipfanexperiences" target="_blank"><img class="svg" src="<?php echo get_template_directory_uri(); ?>/images/google_plus_icon.svg" alt=""><div class="hidden_text">Google Plus</div></a></li>
			                <li><div class="paradeiser_button click-more">&nbsp;</div></li>
			            </ul>
			        </div>
					<!-- <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?> -->
				</nav>
<!-- 				<section id="contact">
					<a class="telephone_link" href="tel:1-800-281-3276">1-800-281-3276</a>
					<a href="facebook.com" target="_blank"><img src="<?php echo get_template_directory_uri()."/images/facebook_icon.png" ?>" /></a>
					<a href="facebook.com" target="_blank"><img src="<?php echo get_template_directory_uri()."/images/twitter_icon.png" ?>" /></a>
					<a href="facebook.com" target="_blank"><img src="<?php echo get_template_directory_uri()."/images/google_plus_icon.png" ?>" /></a>
				</section>
			</div> -->
		</header>
		<div class="clear"></div>
		<div id="container">