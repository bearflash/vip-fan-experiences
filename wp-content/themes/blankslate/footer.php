<div class="clear"></div>
</div>
<footer id="footer">
	<div id="copyright">
		<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
		<div class="social_links">
			<ul>
				<li><a href="https://www.facebook.com/vipfanexperiences/" target="_blank"><img alt="Facebook" src="<?php echo get_template_directory_uri(); ?>/images/facebook_icon.svg" width="35" height="35" /></a></li>
				<li><a href="https://twitter.com/vip_fe" target="_blank"><img alt="Twitter" src="<?php echo get_template_directory_uri(); ?>/images/twitter_icon.svg" width="35" height="35" /></a></li>
				<li><a href="https://plus.google.com/+Vipfanexperiences" target="_blank"><img alt="Google Plus" src="<?php echo get_template_directory_uri(); ?>/images/google_plus_icon.svg" width="35" height="35" /></a></li>
			</ul>
		</div>
		<div class="bbb_logo">
			<a target="_blank" title="VIP Fan Experiences, LLC BBB Business Review" href="http://www.bbb.org/wisconsin/business-reviews/ticket-sales-events/vip-fan-experiences-llc-in-de-pere-wi-1000018738/#bbbonlineclick">
				<img alt="VIP Fan Experiences, LLC BBB Business Review" style="border: 0;" src="<?php echo get_template_directory_uri(); ?>/images/blue-seal-96-50-vip-fan-experiences-llc-1000018738.png" width="96" height="50" />
			</a>
		</div>
		<div class="copyright">
			&copy;&nbsp;Copyright <?php echo date('Y'); ?> Vip Fan Experiences, LLC
		</div>
	</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>