<?php get_header(); ?>
<section id="hero_image entry-content" role="main">
<div class="post_wrapper">
	<div class="post_content col-xs-12">
	<?php
		// Start the loop.
	$site_link = esc_url( get_site_url() );
	while ( have_posts() ) : the_post();
	$cat = get_the_category();
	$cat = $cat[0];
	$cat_name = $cat->category_nicename;
			// Include the single post content template.
	echo '<div class="post_header text_container">';
	echo '<h1 class="page_title">';
	the_title();
	echo '</h1>';
	echo '<div class="post_date">';
	echo " By: ".get_field("author_name")." | ";
	the_time('M jS, Y');
	echo '</div><br />';
	echo '</div>';
			// Include the page content template.
	echo '<div class="content_wrapper col-xs-12">';
	echo '<div class="post_image">';
	the_post_thumbnail('large',array('class'=>'img-responsive'));
	echo '</div>';
	echo '<div class="post_body">';
	the_content();
	echo '</div>';
	echo '</div>';

			// End of the loop.
	endwhile;
	?>
	</div>
	<div class="clear"></div>
<div class="footer">
</div>
</div>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>