<?php
/**
 * Template Name: Posts Page Template
 *
 * @package WordPress
 * @subpackage blankslate
 */
 get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header">
<?php
$image = get_field('hero_image');
$imageid = $image['id'];
$size = 'full'; // (thumbnail, medium, large, full or custom size)
$image_url = wp_get_attachment_image_src( $imageid, $size )[0];
?>
</header>
<section class="entry-content">
<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
<div class="post_wrapper">
	<div class="post_header hero_image <?php echo get_field('hero_image_class'); ?>">
					<div class="hero_fade">
						<div class="page_title"><?php the_title(); ?></div>
						<h1 class="page_description"><?php echo get_field('h1_tagline'); ?></h1>
					</div>
				</div>
	<div class="post_content">
<?php the_content(); ?>
</div>
<?php endwhile; endif; ?>
<div class="posts_wrapper">
		<?php
		$slug_name = get_field("post_cat")[0]->slug;
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$wp_query = new WP_Query(array ( 'category_name' => $slug_name, 'posts_per_page' => 6, 'paged' => $paged ));
		//query_posts( array ( 'category_name' => 'blog_posts', 'posts_per_page' => 4 ) );
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
		echo '<div class="post_wrapper">';
		echo '<div class="post_image col-xs-12 col-sm-12 col-md-5">';
		the_post_thumbnail('large', array('class'=>'img-responsive'));
		echo '</div>';
		echo '<div class="post_body col-xs-12 col-sm-12 col-md-7"><ul>';
		echo '<li class="post_title">';
		the_title();
		echo '</li>';
		echo '<li>';
		the_time('n/j/y');
		echo '</li>';
		echo '<li class="excerpt">';
		the_excerpt();
		echo '</li>';
		echo '</ul></div>';
		echo '</div>';
		  $img_src=get_template_directory_uri().'/images/thin_inset_separator.png';
		  echo '<div class="post_divider col-xs-12 col-sm-12 col-md-12"><img alt="======================" class="img-responsive" src="'.$img_src.'" /></div>';

		// End the loop.
		endwhile;
		?>
		<div class="clear"></div>
		<?php
		if($wp_query->max_num_pages>1){?>
		    <div class="post_pages_nav">
		    <?php
		      if ($paged > 1) { ?>
		        <a href="<?php echo '?paged=' . ($paged -1); //prev link ?>"><</a>
		                        <?php }
		    for($i=1;$i<=$wp_query->max_num_pages;$i++){
		    	if($i != ($wp_query->max_num_pages)){ ?>
		        	<a href="<?php echo '?paged=' . $i; ?>" <?php echo ($paged==$i)? 'class="selected"':'';?>><?php echo $i;?>&nbsp;<span class="green">|</span></a>
		        <?php } else { ?>
		        	<a href="<?php echo '?paged=' . $i; ?>" <?php echo ($paged==$i)? 'class="selected"':'';?>><?php echo $i;?></a>
		        <?php
		    	}
		    }
		    if($paged < $wp_query->max_num_pages){?>
		        <a href="<?php echo '?paged=' . ($paged + 1); //next link ?>">></a>
		    <?php } ?>
		    </div>
		<?php } ?>
	</div>
</div>
</section>
</article>
</section>
<?php get_footer(); ?>