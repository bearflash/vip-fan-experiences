<?php get_header(); ?>
<section id="content" role="main">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<section class="entry-content">
			<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
			<div class="post_wrapper">
				<?php if(get_field('hero_image_class') == '') { ?>
				<div class="post_header no-hero">
					<div>
						<div class="page_title"><?php the_title(); ?></div>
						<h1 class="page_description"><?php echo get_field('h1_tagline'); ?></h1>
					</div>
				</div>
				<?php } else { ?>
				<div class="post_header hero_image <?php echo get_field('hero_image_class'); ?>">
					<div class="hero_fade">
						<div class="page_title"><?php the_title(); ?></div>
						<h1 class="page_description"><?php echo get_field('h1_tagline'); ?></h1>
					</div>
				</div>
				<?php } ?>
				<?php if(get_the_ID() != "12") {?>
				<div class="quote_wrapper">
					<a href="/request-a-quote">
						<div class="btn wide btn-primary">Request A Quote</div>
					</a>
				</div>
				<?php } ?>
				<div class="post_content">
					<?php the_content(); ?>
				</div>
			</div>
		</section>
	</article>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>