<?php get_header(); ?>
<section id="content" role="main">
<article id="post-0" class="post not-found">
<section class="entry-content">
	<div class="post_wrapper">
		<div class="post_header hero_image" style="background-image:url('<?php echo $image_url; ?>');">
			<div class="hero_fade">
				<div class="page_title">404 Not Found</div>
			</div>
		</div>
		<div class="post_content">
			<h2>Sorry the page you requested was not found.</h2>
		</div>
	</div>
</section>
</article>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>