$ = jQuery;
$(document).ready(function() {
	$('.click-more').click(function(){
		$('#paradeiser-more').toggleClass('paradeiser_hidden');
	});

    svg_convertor();

});

function svg_convertor() {
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');

            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Replace image with new SVG
            $img.replaceWith($svg);

        }, 'xml');

    });
}

// enquire.register("screen and (max-width: 992px)", {
//     match : function() {
//         $(document).ready(function() {
//             var bg = $(".hero_image").css('background-image');
//             bg = bg.replace('url(','').replace(')','');
//             var pos = bg.lastIndexOf(".");
//             var bg_mobile = bg.slice(0, pos) + "_mobile" + bg.slice(pos);
//             $(".hero_image").css('background-image', "url("+bg_mobile+")");
//         });
//     },  
//     unmatch : function() {
//         $(document).ready(function() {
//             var bg = $(".hero_image").css('background-image');
//             bg = bg.replace('url(','').replace(')','').replace('_mobile','');
//             $(".hero_image").css('background-image', "url("+bg+")");
//         });
//     }
// });