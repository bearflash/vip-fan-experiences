jQuery( document ).ready(function() {
	form_validation_init();
    add_event_listeners();
    add_date_pickers();
    generate_section_dropdown();
});

function add_event_listeners() {
	jQuery(".airfare_start").click(function() {
		jQuery(".airfare_body").css("display", "block");
		jQuery(".airfare_start").css("display", "none");
		jQuery(".airfare_close").css("display", "block");
		jQuery("#airfare_clicked").val("true");
	});

	jQuery(".airfare_close").click(function() {
		jQuery(".airfare_body").css("display", "none");
		jQuery(".airfare_start").css("display", "block");
		jQuery(".airfare_close").css("display", "none");
		jQuery("#airfare_clicked").val("false");
	})

	jQuery(".sport_type").change(function() {
		generate_section_dropdown();
	});
}

function add_date_pickers() {
	jQuery("#event_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		minDate: 0,
		onSelect: function(selected) {
	        jQuery('#checkin_date').datepicker("option", "maxDate",  jQuery("#event_date").datepicker('getDate') );
	        jQuery('#event_date').valid();
	    }
	});
	jQuery("#checkin_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		minDate: 0,
		onSelect: function(selected) {
			var inDate = jQuery("#checkin_date").datepicker('getDate');
			inDate.setDate(inDate.getDate()+1);
	        jQuery('#checkout_date').datepicker("option", "minDate",  inDate );
	        jQuery('#departure_date').datepicker("option", "maxDate",  jQuery("#checkin_date").datepicker('getDate') );
	        jQuery('#checkin_date').valid();
	    }
	});
	jQuery("#checkout_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		minDate: 0
	}).on('change', function() {
		jQuery('#return_date').datepicker("option", "maxDate",  jQuery("#checkout_date").datepicker('getDate') );
        jQuery('#checkout_date').valid();
    });
	jQuery("#departure_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		minDate: 0,
		onSelect: function(selected) {
			var inDate = jQuery("#departure_date").datepicker('getDate');
			inDate.setDate(inDate.getDate()+1);
	        jQuery('#return_date').datepicker("option", "minDate",  inDate );
	        jQuery('#departure_date').valid();
	    }
	});
	jQuery("#return_date").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd',
		minDate: 0
	}).on('change', function() {
        jQuery('#return_date').valid();
    });
}

function generate_section_dropdown() {
	jQuery(".sport_type option:selected").each(function() {
		var selector = jQuery(this).text();
		if(selector == "NFL/NCAA Football" || selector == "NBA/NCAA Basketball" || selector == "NHL" || selector == "Soccer") {
			jQuery(".section_type select").html("<option value='default'>Select a Section</option><option value='Upper End'>Upper End</option><option value='Upper Sideline'>Upper Sideline</option><option value='Lower End'>Lower End</option><option value='Lower Sideline'>Lower Sideline</option><option value='Club'>Club</option>");
		} else if(selector == "MLB") {
			jQuery(".section_type select").html("<option value='default'>Select a Section</option><option value='Bleachers'>Bleachers</option><option value='Upper Outfield'>Upper Outfield</option><option value='Upper Infield'>Upper Infield</option><option value='Lower Outfield'>Lower Outfield</option><option value='Lower Infield'>Lower Infield</option><option value='Club'>Club</option>");
		} else if(selector == "Golf"){
			jQuery(".section_type select").html("<option value='default'>Select a Section</option><option value='General Admission'>General Admission</option><option value='Club'>Club</option>");
		} else {
			jQuery(".section_type select").html("<option value='choose'>Choose a sport first</option>");
		}
	});
}

function form_validation_init() {
	jQuery.validator.addMethod("valueNotEquals", function(value, element, arg){
	  return arg != value;
	}, "Value must not equal arg.");

	jQuery("#contact_form").validate({
		rules: {
			first_name: {
				required: true,
				minlength:2
			},
			last_name: {
				required: true,
				minlength:2
			},
			email: {
				required: true,
				email: true
			},
			email_confirm: {
				equalTo: "#email"
			},
			ticket_count: {
				valueNotEquals: "default"
			},
			sport_type: {
				valueNotEquals: "default"
			},
			section: {
				valueNotEquals: "default"
			},
			event_team: {
				required: true
			},
			event_city: {
				required: true
			},
			event_date: {
				required: true,
				dateISO: true
			},
			checkin_date: {
				required: true,
				dateISO: true
			},
			checkout_date: {
				required: true,
				dateISO: true
			},
			room_number: {
				valueNotEquals: "default"
			},
			bed_number: {
				valueNotEquals: "default"
			},
			hotel_quality: {
				valueNotEquals: "default"
			},
			flight_ticket_count: {
				valueNotEquals: "default"
			},
			departure_city: {
				required: {
					depends: function(element){
						return jQuery("#airfare_clicked").val() != false
					}
				}
			},
			departure_date: {
				required: {
					depends: function(element){
						return jQuery("#airfare_clicked").val() != false
					}
				},
				dateISO: true
			},
			destination_city: {
				required: {
					depends: function(element){
						return jQuery("#airfare_clicked").val() != false
					}
				}
			},
			return_date: {
				required: {
					depends: function(element){
						return jQuery("#airfare_clicked").val() != false
					}
				},
				dateISO: true
			}

		},
		messages: {
			first_name: {
				required: "Please enter your first name.",
				minlength: "Your username must consist of at least 2 characters"
			},
			last_name: {
				required: "Please enter your last name.",
				minlength: "Your username must consist of at least 2 characters"
			},
			email: "Please enter a valid email address",
			email_confirm: "Both email address must match",
			ticket_count: {
				valueNotEquals: "Please enter the number of event tickets"
			},
			sport_type: {
				valueNotEquals: "Please select a sport"
			},
			section: {
				valueNotEquals: "Please select a section"
			},
			event_team: "Please enter a team name",
			event_city: "Please enter a city",
			event_date: {
				required: "Please select a date for your event",
				dateISO: "Please enter a valid date"
			},
			checkin_date: {
				required: "Please select a check in date",
				dateISO: "Please enter a valid date"
			},
			checkout_date: {
				required: "Please select a check out date",
				dateISO: "Please enter a valid date"
			},
			room_number: {
				valueNotEquals: "Please select number of rooms desired"
			},
			bed_number: {
				valueNotEquals: "Please select number of beds desired per room"
			},
			hotel_quality: {
				valueNotEquals: "Please select hotel quality desired"
			},
			flight_ticket_count: {
				valueNotEquals: "Please enter the number of airline tickets"
			},
			departure_city: "Please enter a departure city",
			departure_date: {
				required: "Please select a date for your departure",
				dateISO: "Please enter a valid date"
			},
			destination_city: "Please enter a destination city",
			return_date: {
				required: "Please select a date for your return",
				dateISO: "Please enter a valid date"
			}
		}
	});
}