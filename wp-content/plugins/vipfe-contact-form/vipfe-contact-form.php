<?php

/**
* Plugin Name: VIPFE Contact form
* Plugin URI: http://vipfanexperiences.com/
* Description: Contact form plugin for VIP Fan Experiences site
* Version: 1.0
* Author: Joe Bayer
* Author URI: www.joebayermedia.com
*/



global $db_version;

$db_version = '1.0';



function db_create() {

	global $wpdb;

	global $db_version;



	$table_name = $wpdb->prefix . 'vipfecontact';

	

	$charset_collate = $wpdb->get_charset_collate();



	$sql = "CREATE TABLE $table_name (

		id mediumint(9) NOT NULL AUTO_INCREMENT,

		firstname varchar(55) NOT NULL,

		lastname varchar(55) NOT NULL,

		email varchar(55) NOT NULL,

		telephone varchar(55),

		ticketcount varchar(55) NOT NULL,

		sport varchar(55) NOT NULL,

		section varchar(55) NOT NULL,

		team varchar(55) NOT NULL,

		eventcity varchar(55) NOT NULL,

		eventdate varchar(55) NOT NULL,

		hotelcheckin varchar(55) NOT NULL,

		hotelcheckout varchar(55) NOT NULL,

		numberofrooms varchar(55) NOT NULL,

		numberofbeds varchar(55) NOT NULL,

		hotelquality varchar(55) NOT NULL,

		flighttickets varchar(55),

		departurecity varchar(55),

		departuredate varchar(55),

		departuretime varchar(55),

		destinationcity varchar(55),

		returndate varchar(55),

		returntime varchar(55),

		emailoptin varchar(55) NOT NULL,

		UNIQUE KEY id (id)

	) $charset_collate;";



	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	dbDelta( $sql );



	add_option( 'jal_db_version', $db_version );

}



register_activation_hook(__FILE__,'db_create');



add_action('admin_menu', 'my_plugin_menu');



function my_plugin_menu() {

	add_menu_page('VIPFE Contact Form', 'VIPFE Contact Form Settings', 'administrator', 'vipfe-contact-form', 'vipfe_contact_settings_page', 'dashicons-vipfe-contact');

}



function vipfe_contact_settings_page() {

	echo '<div class="wrap">';

	echo '<h2>Staff Details</h2>';



	echo '<form method="post" action="options.php">';

	    settings_fields( 'vipfe-contact-settings-group' );

	    do_settings_sections( 'vipfe-contact-settings-group' );

	    $accountant_name = esc_attr( get_option('accountant_name') );

	    $accountant_phone = esc_attr( get_option('accountant_phone') );

	    $accountant_email = esc_attr( get_option('accountant_email') );

	    echo '<table class="form-table">';

	        echo '<tr valign="top">';

	        echo '<th scope="row">Accountant Name</th>';

	        echo '<td><input type="text" name="accountant_name" value="'.$accountant_name.'" /></td>';

	        echo '</tr>';

	         

	        echo '<tr valign="top">';

	        echo '<th scope="row">Accountant Phone Number</th>';

	        echo '<td><input type="text" name="accountant_phone" value="'.$accountant_phone.'" /></td>';

	        echo '</tr>';

	        

	        echo '<tr valign="top">';

	        echo '<th scope="row">Accountant Email</th>';

	        echo '<td><input type="text" name="accountant_email" value="'.$accountant_email.'" /></td>';

	        echo '</tr>';

	    echo '</table>';

	    

	    submit_button();



	echo '</form>';

	echo '</div>';

}



add_action( 'admin_init', 'vipfe_contact_settings' );



function vipfe_contact_settings() {

	register_setting( 'vipfe-contact-settings-group', 'accountant_name' );

	register_setting( 'vipfe-contact-settings-group', 'accountant_phone' );

	register_setting( 'vipfe-contact-settings-group', 'accountant_email' );

}



add_shortcode("vipfe_form", "init_form");



function init_form (){

	if(!$_POST['form_submit']){

		echo "<div class='vipfe_form'>";

			echo "<form method='post' id='contact_form'>";

				echo "<input type='hidden' name='form_submit' value='true'>";

				echo "<div class='contact section'>";

					echo "<div class='contact title'>Contact Information</div><hr />";

					echo "<div class='input_label name_inputs'><span>Name: </span><div class='col-xs-12 col-sm-4'><input type='text' name='first_name' placeholder='First' id='first_name'></div><div class='col-xs-12 col-sm-4'><input type='text' name='last_name' placeholder='Last' id='last_name'></div></div>";

					echo "<div class='clear'></div>";

					echo "<div class='input_label email_input col-xs-12 col-sm-4'><span>Email: </span><input type='email' name='email' id='email' placeholder='name@example.com'></div>";

					echo "<div class='input_label email_input col-xs-12 col-sm-4'><span>Confirm Email: </span><input type='email' name='email_confirm' id='email_confirm' placeholder='name@example.com'></div>";

					echo "<div class='input_label col-xs-12 col-sm-4'><span>Telephone: </span><input type='tel' name='telephone' placeholder='XXX-XXX-XXXX'></div>";

				echo "</div><div class='clear'></div>";

				echo "<div class='tickets section'>";

					echo "<div class='tickets title'>Tickets</div><hr />";

					echo "<div class='input_label travelers col-xs-12 col-sm-4'><span>Number of Tickets: </span><select name='ticket_count' id='ticket_count'>";

						echo "<option value='default'>Select Number</option>";

						echo "<option value='1'>1</option>";

						echo "<option value='2'>2</option>";

						echo "<option value='3'>3</option>";

						echo "<option value='4'>4</option>";

						echo "<option value='5'>5</option>";

						echo "<option value='6'>6</option>";

					echo "</select></div>";

					echo "<div class='large_groups travelers col-xs-12 col-sm-4'>*For groups larger than six people, please contact us directly at <a href='mailto:bookings@vipfanexperiences.com'>bookings@vipfanexperiences.com</a>.</div>";

					echo "<div class='clear'></div>";

					echo "<div class='input_label col-xs-12 col-sm-4'><span>Sport: </span><select name='sport_type' id='sport_type' class='sport_type'>";

						echo "<option value='default'>Select a Sport</option>";

						echo "<option value='Football'>NFL/NCAA Football</option>";

						echo "<option value='Baseball'>MLB</option>";

						echo "<option value='Basketball'>NBA/NCAA Basketball</option>";

						echo "<option value='Hockey'>NHL</option>";

						echo "<option value='Soccer'>Soccer</option>";

						echo "<option value='Golf'>Golf</option>";

					echo "</select></div>";

					echo "<div class='input_label section_type col-xs-12 col-sm-4'><span>Section: </span>";

						echo "<select name='section' id='section'>";

						echo "<option value='choose'>Choose a sport first</option>";

					echo "</select>";

					echo "</div>";

					echo "<div class='clear'></div>";

					echo "<div class='input_label col-xs-12 col-sm-4'><span>Event Team: </span><input type='text' name='event_team' id='event_team' placeholder='ex. San Diego Chargers'></div>";

					echo "<div class='input_label col-xs-12 col-sm-4'><span>Event City: </span><input type='text' name='event_city' id='event_city' placeholder='ex. San Diego'></div>";

					echo "<div class='input_label col-xs-12 col-sm-4'><span>Event Date: </span><input type='text' name='event_date' id='event_date'></div>";

				echo "</div>";

				echo "<div class='clear'></div>";

				echo "<div class='hotel section'>";

					echo "<div class='hotel title'>Hotels</div><hr />";

					echo "<div><div class='input_label hotel_dates col-xs-12 col-sm-4'><span>Check in Date: </span><input type='text' name='checkin_date' id='checkin_date'></div>";

					echo "<div class='input_label hotel_dates col-xs-12 col-sm-4'><span>Check out Date: </span><input type='text' name='checkout_date' id='checkout_date'></div></div>";

					echo "<div class='clear'></div>";

					echo "<div><div class='input_label hotel_details col-xs-12 col-sm-4'><span>Number of Rooms: </span><select name='room_number' id='room_number'>";

						echo "<option value='default'>Select Rooms</option>";

						echo "<option value='1'>1</option>";

						echo "<option value='2'>2</option>";

						echo "<option value='3'>3</option>";

						echo "<option value='4'>4</option>";

						echo "<option value='5'>5</option>";

						echo "<option value='6'>6</option>";

					echo "</select></div>";

					echo "<div class='input_label hotel_details col-xs-12 col-sm-4'><span>Number of Beds(per room): </span><select name='bed_number' id='bed_number'>";

						echo "<option value='default'>Select Beds</option>";

						echo "<option value='1'>1</option>";

						echo "<option value='2'>2</option>";

					echo "</select></div></div>";

					echo "<div class='input_label col-xs-12 col-sm-4'><span>Hotel Quality: </span><select name='hotel_quality' id='hotel_quality'>";

						echo "<option value='default'>Select Quality</option>";

						echo "<option value='Moderate'>Moderate (Holiday Inn Express, Best Western Plus, etc)</option>";

						echo "<option value='Upper Moderate'>Upper Moderate (Hyatt, Sheraton, Westin, etc)</option>";

						echo "<option value='Luxury'>Luxury (Four Seasons, Omni, Fairmont, etc)</option>";

					echo "</select></div>";

					echo "<div class='clear'></div>";

				echo "</div>";

				/* echo "<div class='airfare section'>";

					echo "<div class='airfare title'>Airfare</div> <div class='airfare subtitle'>Click below to add airfare to your package estimate</div><hr />";

					echo "<div class='airfare_start'>Click to Start</div>";

					echo "<input type='hidden' name='airfare_clicked' id='airfare_clicked' value='false'>";

					echo "<div class='airfare_body'><div class='input_label travelers' style='display:block;'><span>Number of Tickets: </span><select name='flight_ticket_count' id='flight_ticket_count'>";

						echo "<option value='default'>Select Number</option>";

						echo "<option value='1'>1</option>";

						echo "<option value='2'>2</option>";

						echo "<option value='3'>3</option>";

						echo "<option value='4'>4</option>";

						echo "<option value='5'>5</option>";

						echo "<option value='6'>6</option>";

					echo "</select></div>";

					echo "<div class='input_label departure'><span>Departure City: </span><input type='text' name='departure_city' placeholder='ex. Chicago' id='departure_city'></div>";

					echo "<div class='input_label departure'><span>Departure Date: </span><input type='text' name='departure_date' id='departure_date'></div>";

					echo "<div class='input_label departure'><span>Time: </span><select name='departure_time' id='departure_time'>";

						echo "<option value='Anytime'>Anytime</option>";

						echo "<option value='5am-7am'>5AM-7AM</option>";

						echo "<option value='7am-9am'>7AM-9AM</option>";

						echo "<option value='9am-1pm'>9AM-1PM</option>";

						echo "<option value='1pm-6pm'>1PM-6PM</option>";

						echo "<option value='6pm-10pm'>6PM-10PM</option>";

					echo "</select></div>";

					echo "<div class='input_label destination'><span>Destination City: </span><input type='text' name='destination_city' placeholder='ex. New York' id='destination_city'></div>";

					echo "<div class='input_label destination'><span>Return Date: </span><input type='text' name='return_date' id='return_date'></div>";

					echo "<div class='input_label destination'><span>Time: </span><select name='return_time' id='return_time'>";

						echo "<option value='Anytime'>Anytime</option>";

						echo "<option value='5am-7am'>5AM-7AM</option>";

						echo "<option value='7am-9am'>7AM-9AM</option>";

						echo "<option value='9am-1pm'>9AM-1PM</option>";

						echo "<option value='1pm-6pm'>1PM-6PM</option>";

						echo "<option value='6pm-10pm'>6PM-10PM</option>";

					echo "</select></div>";

					echo "<div class='airfare_close' style='display:none;'>Click to Close</div></div>";

				echo "</div>"; */

				echo "<div class='submit_button section'>";

					echo "<button class='submit'>Submit</button>";

				echo "</div>";

			echo "</form>";

		echo "</div>";

	} else {

		$first_name = htmlspecialchars($_POST['first_name']);

		$last_name = htmlspecialchars($_POST['last_name']);

		$email = htmlspecialchars($_POST['email']);

		$telephone = htmlspecialchars($_POST['telephone']);

		$ticket_count = htmlspecialchars($_POST['ticket_count']);

		$sport_type = htmlspecialchars($_POST['sport_type']);

		$section = htmlspecialchars($_POST['section']);

		$event_team = htmlspecialchars($_POST['event_team']);

		$event_city = htmlspecialchars($_POST['event_city']);

		$event_date = htmlspecialchars($_POST['event_date']);

		$checkin_date = htmlspecialchars($_POST['checkin_date']);

		$checkout_date = htmlspecialchars($_POST['checkout_date']);

		$room_number = htmlspecialchars($_POST['room_number']);

		$bed_number = htmlspecialchars($_POST['bed_number']);

		$hotel_quality = htmlspecialchars($_POST['hotel_quality']);

		$email_contact = htmlspecialchars($_POST['opt_in']);



		if($email_contact == 'on') {

			$email_contact = TRUE;

		} else {

			$email_contact = FALSE;

		}



		$event_date = date("F j, Y", strtotime($event_date));

		$checkin_date = date("F j, Y", strtotime($checkin_date));

		$checkout_date = date("F j, Y", strtotime($checkout_date));



		$airfare_clicked = htmlspecialchars($_POST['airfare_clicked']);

		if($airfare_clicked == "true") {

			$flight_ticket_count = htmlspecialchars($_POST['flight_ticket_count']);

			$departure_city = htmlspecialchars($_POST['departure_city']);

			$departure_date = htmlspecialchars($_POST['departure_date']);

			$departure_time = htmlspecialchars($_POST['departure_time']);

			$destination_city = htmlspecialchars($_POST['destination_city']);

			$return_date = htmlspecialchars($_POST['return_date']);

			$return_time = htmlspecialchars($_POST['return_time']);

			$departure_date = date("F j, Y", strtotime($departure_date));

			$return_date = date("F j, Y", strtotime($return_date));

		}

		echo '<div class="submit section">Thank you for submitting your contact request with VIP Fan Experiences! One of our representatives will get in touch with your shortly with a price and option to purchase! Thank you!</div>';



		$subj = 'Requested Quote from ' . $first_name . ' ' . $last_name;

		$body = '<html><body style="background:#ffffff;border-spacing:0px;border-collapse:collapse;font-family:Arial;padding-bottom:0px;padding-left:0px;padding-right:0px;padding-top:0px;margin-bottom:0px;margin-left:0px;margin-right:0px;margin-top:0px;" bgcolor="#ffffff">';

		$body .= '<table cellpadding="1" cellspacing="0" border="1" width="480" bgcolor="#FFFFFF" style="font-size:16px; margin:0 auto;-webkit-text-size-adjust:none;">';

		$body .= '<tr><td colspan="2" width="480" style="text-align:center; font-size:24px;">Contact Information</td>';

		$body .= '<tr><td width="240">Name</td><td width="240">'. $first_name . ' ' . $last_name . '</td></tr>';

		$body .= '<tr><td>Email</td><td>' . $email . '</td>';

		if($telephone != '') {

			$body .= '<tr><td>Phone</td><td>' . $telephone . '</td>';

		}

		$body .= '<tr><td colspan="2" width="480" style="text-align:center; font-size:24px;">Event Details</td>';

		$body .= '<tr><td>Number of Tickets</td><td>' . $ticket_count . '</td>';

		$body .= '<tr><td>Sport</td><td>' . $sport_type . '</td>';

		$body .= '<tr><td>Section</td><td>' . $section . '</td>';

		$body .= '<tr><td>Team</td><td>' . $event_team . '</td>';

		$body .= '<tr><td>City</td><td>' . $event_city . '</td>';

		$body .= '<tr><td>Date of Event</td><td>' . $event_date . '</td>';

		$body .= '<tr><td colspan="2" width="480" style="text-align:center; font-size:24px;">Hotel Details</td>';

		$body .= '<tr><td>Check In</td><td>' . $checkin_date . '</td>';

		$body .= '<tr><td>Check Out</td><td>' . $checkout_date . '</td>';

		$body .= '<tr><td>Number of Rooms</td><td>' . $room_number . '</td>';

		$body .= '<tr><td>Number of Beds</td><td>' . $bed_number . '</td>';

		$body .= '<tr><td>Quality</td><td>' . $hotel_quality . '</td>';

		if($airfare_clicked == "true") {

			$body .= '<tr><td colspan="2" width="480" style="text-align:center; font-size:24px;">Flight Details</td>';

			$body .= '<tr><td>Number of Tickets</td><td>' . $flight_ticket_count . '</td>';

			$body .= '<tr><td>Departure City</td><td>' . $departure_city . '</td>';

			$body .= '<tr><td>Departure Date</td><td>' . $departure_date . '</td>';

			$body .= '<tr><td>Departure Time</td><td>' . $departure_time . '</td>';

			$body .= '<tr><td>Destination City</td><td>' . $destination_city . '</td>';

			$body .= '<tr><td>Return Date</td><td>' . $return_date . '</td>';

			$body .= '<tr><td>Return Time</td><td>' . $return_time . '</td>';

		}

		$body .= '</table>';

		$body .= '</html></body>';

		$headers[] = 'From: VIP Fan Experiences <donotreply@vipfanexperiences.com>' . "\r\n";

		$headers[] = 'Bcc: VIPFE Contact <bookings@vipfanexperiences.com>';

		$headers[] = 'Content-Type: text/html; charset=UTF-8';

		wp_mail( $email, $subj, $body, $headers );



		global $wpdb;

		$table = $wpdb->prefix . 'vipfecontact';



		$data['firstname'] = $first_name;

		$data['lastname'] = $last_name;

		$data['email'] = $email; 

		if($telephone != '') {

			$data['telephone'] = $telephone;

		}

		$data['ticketcount'] = $ticket_count;

		$data['sport'] = $sport_type;

		$data['section'] = $section;

		$data['team'] = $event_team;

		$data['eventcity'] = $event_city;

		$data['eventdate'] = $event_date;

		$data['hotelcheckin'] = $checkin_date;

		$data['hotelcheckout'] = $checkout_date;

		$data['numberofrooms'] = $room_number;

		$data['numberofbeds'] = $bed_number;

		$data['hotelquality'] = $hotel_quality;

		if($airfare_clicked == "true") {

			$data['flighttickets'] = $flight_ticket_count;

			$data['departurecity'] = $departure_city;

			$data['departuredate'] = $departure_date;

			$data['departuretime'] = $departure_time;

			$data['destinationcity'] = $destination_city;

			$data['returndate'] = $return_date;

			$data['returntime'] = $return_time;

		}

		$data['emailoptin'] = $email_contact;

		$wpdb->insert($table, $data);



	}

}



add_action( 'wp_enqueue_scripts', 'safely_add_stylesheet' );



function safely_add_stylesheet() {

	wp_enqueue_script('jquery-ui-datepicker');

	wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

    wp_enqueue_style("form-style", plugins_url("css/form.css", __FILE__), FALSE );

    wp_enqueue_script("form-script", plugins_url("js/form.js", __FILE__), array('jquery', 'jquery-ui-core'), '', TRUE );

    wp_enqueue_script("form-validation", plugins_url("js/validation/dist/jquery.validate.js", __FILE__), array('jquery', 'jquery-ui-core'), '', TRUE );

}x



?>