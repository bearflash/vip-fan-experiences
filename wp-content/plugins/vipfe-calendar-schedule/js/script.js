function init_calendar(event_data, team_id, team_abbr) {
	var first_game = event_data[0].event_date;
	var today = moment().format('Y-MM');
	var events = {}
	for(var i=0; i<event_data.length;i++){
		if(team_id == event_data[i].home_team_id) {
			events[event_data[i].event_date] = {"number": "H","url": "http://vipfanexperiences.com/raq?event_id="+event_data[i].id, "class": "home_game", "opponent": "vs&nbsp;"+team_abbr[i].abbr_name};
		} else {
			events[event_data[i].event_date] = {"number": "A","url": "http://vipfanexperiences.com/raq?event_id="+event_data[i].id, "class": "away_game", "opponent": "at&nbsp;"+team_abbr[i].abbr_name};
		}
	}
	$(".responsive-calendar").responsiveCalendar({
		time: today,
		events,
		startFromSunday: true,
		allRows: false
	});
}