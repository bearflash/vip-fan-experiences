<?php

/**
* Plugin Name: VIPFE Schedule Calendar
* Plugin URI: http://vipfanexperiences.com/
* Description: Schedule Calendar plugin for VIP Fan Experiences site
* Version: 1.0
* Author: Joe Bayer
* Author URI: www.joebayermedia.com
*/

include( $_SERVER['DOCUMENT_ROOT'].'/wp-content/plugins/vipfe-calendar-schedule/model/data.php');

global $db_version;
$db_version = '1.0';



add_shortcode("vipfe_calendar", "init_calendar");



function init_calendar ($atts){

	$output = '';
 
    $pull_team_atts = shortcode_atts( array(
        'team' => 'Minnesota Twins',
    ), $atts );

    $team_name = wp_kses_post( $pull_team_atts[ 'team' ] );

    $output .= '<h2>'.$team_name.'</h2>';
	$output .= '<!-- Responsive calendar - START -->';
	$output .= '<div class="responsive-calendar">';
	$output .= '<div class="controls">';
	$output .= '<a class="pull-left" data-go="prev"><div class="btn btn-primary">Prev</div></a>';
	$output .= '<h4><span data-head-year></span> <span data-head-month></span></h4>';
	$output .= '<a class="pull-right" data-go="next"><div class="btn btn-primary">Next</div></a>';
	$output .= '</div><hr/>';
	$output .= '<div class="day-headers">';
	$output .= '<div class="day header">Sun</div>';
	$output .= '<div class="day header">Mon</div>';
	$output .= '<div class="day header">Tue</div>';
	$output .= '<div class="day header">Wed</div>';
	$output .= '<div class="day header">Thu</div>';
	$output .= '<div class="day header">Fri</div>';
	$output .= '<div class="day header">Sat</div>';
	$output .= '</div>';
	$output .= '<div class="days" data-group="days">';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<!-- Responsive calendar - END -->';

	$team_id = get_team_id($team_name);
	$game_data = get_events($team_id);
	$team_abbr = get_team_abbr($team_id);
	$game_data = json_encode($game_data);
	$team_abbr = json_encode($team_abbr);
	$output .= '<script type="text/javascript" src="'.plugins_url("js/moment.min.js", __FILE__).'"></script>';
	$output .= '<script type="text/javascript" src="'.plugins_url("calendar/js/responsive-calendar.js", __FILE__).'"></script>';
	$output .= '<script type="text/javascript" src="'.plugins_url("js/script.js", __FILE__).'"></script>';
	$output .= '<script type="text/javascript">init_calendar('.$game_data.', '.$team_id.', '.$team_abbr.')</script>';

	return $output;
}

add_action( 'wp_enqueue_scripts', 'add_stylesheet' );

function add_stylesheet() {

	wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

    wp_enqueue_style("form-calendar-style", plugins_url("calendar/css/responsive-calendar.css", __FILE__), FALSE );

}



?>